<?php

defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

class Pccaiji_topic extends CI_Controller {
	var $whitelist;
	function __construct() {
		$this->whitelist = "index,clist,addtopic";
		parent::__construct ();

		$this->load->model ( 'category_model' );
		$this->load->model ( 'topic_model' );
		$this->load->model ( 'user_model' );
		if ($this->setting ['xunsearch_open']) {
			require_once $this->setting ['xunsearch_sdk_file'];
			$xs = new XS ( 'topic' );
			$this->search = $xs->search;
			$this->index = $xs->index;
		}
	}

	function index() {
		$file = $this->getfolders ();

	}
	function getfolders() {
		$file = array ();
		$file_dir = "caijiimage";
		$shili = $file_dir;
		if (! file_exists ( $shili )) {
			return '0';
		} else {
			$i = 0;

			if (is_dir ( $shili )) { //检测是否是合法目录
				if ($shi = opendir ( $shili )) { //打开目录
					while ( $li = readdir ( $shi ) ) { //读取目录


						if (strpos ( $li, 'jpg' ) > 0 || strpos ( $li, 'png' ) > 0)
							array_push ( $file, $li );

					}
				}
			} //输出目录中的内容


			closedir ( $shi );
			return $file;
		}
	}
	function removeLinks($str) {
		if (empty ( $str ))
			return '';
		$str = preg_replace ( '/(http)(.)*([a-z0-9\-\.\_])+/i', '', $str );
		return $str;
	}
	function addtopic() {


		$code = $this->setting ['hct_logincode'] != null ? $this->setting ['hct_logincode'] : rand ( 111111, 9999999 );
		if ($this->input->post ( 'title' ) != null) {


			$classid = $this->input->post ( 'classid' );
			$title = trim($this->input->post ( 'title' ));
			$content = htmlspecialchars_decode($_POST ['content']);
			$image = $_POST['image'];
			$viewtime = strtotime ( $_POST ['viewtime'] );
			$uidwf = intval ( $_POST ['uid'] );
			$views = intval ( $_POST ['views'] );
			$id = intval ( $_POST ['id'] );
			$article = $this->topic_model->get ($id );
			if ($code != $this->input->post ( 'articlevalue' )) {

				echo '没有发布权限!';
				exit ();
			}
			if ($article['title'] != null) {

				echo '文章已存在，发布时间为' . $article ['viewtime'];
				exit ();
			} else {
				$content = preg_replace ( "#<a[^>]*>(.*?)</a>#is", "$1", $content );
				$userone = $this->user_model->get_by_uid ( $uidwf );
				if ($userone['uid']) {
					$uid = $userone ['uid'];
					$username = $userone ['username'];
				} else {
					$userlist = $this->user_model->get_caiji_list ( 0, 40 );
					$mwtuid = array_rand ( $userlist, 1 );
					$uid = $userlist [$mwtuid] ['uid'];
					$username = $userlist [$mwtuid] ['username'];
				}
				if ($image) {
					$img_url = $image;
				} else {
					$file = $this->getfolders ();

					//  for($i=1;$i<=60;$i++){
					//array_push($classarr, $i);
					// }
					$url = 'http://www.ask2.cn/data/attach/topic/topic6DYxVY.jpg';
					$img_url = getfirstimg ( $content );

					if ($file != '0') {
						$mwtfid = array_rand ( $file, 1 );

						$url = SITE_URL . 'static/caijiimage/' . $file [$mwtfid];
					}
					if ($img_url == "") {
						$img_url = $url;
					}
				}


				if (trim ( $content ) != '') {
					if (! $views) {
						$views = rand ( 1000, 30000 );
					}
					if (! $viewtime) {
						$randnum = rand ( 10, 70 );
						$viewtime = strtotime ( "-$randnum minute" );
					}

					$aid = $this->addtopicwenzhang ($id, $title, $content, $img_url, $username, $uid, $views, $classid ,$viewtime);
					if ($aid > 0) {
						//$this->load("topic_tag");
						// $tagarr= dz_segment($this->post['title'],$this->post['content']);
						// $tagarr && $_ENV['topic_tag']->multi_add(array_unique($tagarr),  $aid);
						echo '发布成功---哈哈';
					} else {

						echo "发布失败";

					}
				} else {
					echo "发布失败,内容不能为空";
				}

			}

		} else {
			echo '标题不能为空';
		}

	}
	function addtopicwenzhang($id,$title, $desc, $image, $author, $authorid, $views, $articleclassid,$creattime) {

		$desc = $this->get_otherimgouter ( $desc );
		//$desc = filter_otherimgouter ( $desc );
		//$_image = getfirstimg ( $desc );


		$data = array ('id' => $id,'title' => $title, 'describtion' => $desc, 'image' => $image, 'author' => $author, 'authorid' => $authorid, 'views' => $views, 'articleclassid' => $articleclassid, 'viewtime' => $creattime );
		$this->db->insert ( 'topic', $data );
		runlog('query',$this->db->last_query());
		$aid = $this->db->insert_id ();
		//$ataglist = dz_segment ( urlencode ( $title ) );

		//$ataglist && $this->topic_tag_model->multi_add ( array_unique ( $ataglist ), $aid );
		$this->db->query ( "UPDATE " . $this->db->dbprefix . "user SET articles=articles+1 WHERE  uid =" . $authorid );
		//发布文章，添加积分
		//$this->credit ( $this->user ['uid'], $this->setting ['credit1_article'], $this->setting ['credit2_article'], 0, 'addarticle' );

		if ($this->setting ['xunsearch_open'] && $aid) {
			$topic = array ();
			$topic ['id'] = $aid;
			$topic ['views'] = $views;
			$topic ['articles'] = 0;
			$topic ['likes'] = 0;
			$topic ['articleclassid'] = $articleclassid;
			$topic ['title'] = checkwordsglobal ( $title );
			$topic ['describtion'] = checkwordsglobal ( $desc );
			$topic ['author'] = $author;
			// $topic['price'] = $price;
			$topic ['authorid'] = $authorid;
			$topic ['viewtime'] = $creattime;

			$doc = new XSDocument ();
			$doc->setFields ( $topic );
			$this->index->add ( $doc );
		}
		return $aid;
	}
//过滤图片其它url
function get_otherimgouter($content) {
	if (! function_exists ( 'file_get_html' )) {
		require_once (BASEPATH . 'helpers/simple_html_dom_helper.php');
	}
	$html = str_get_html ( $content );
	$ret = $html->find ( 'img' );
	foreach ( $ret as $a ) {
		if (FALSE===strpos ($a->src, 'http' )) {
			$a->src = "http://img.52fuqing.com".$a->src;

		}
	}
	$content = $html->save ();
	$html->clear ();
	return $content;

}

	function clist() {

		$cid = intval ( $this->uri->segment ( 3 ) ) ? $this->uri->segment ( 3 ) : 'all';
		$navlist = $this->category_model->list_by_pid ( 0 ); //获取导航
		$cat_string = '';
		foreach ( $navlist as $nav ) {
			$cat_string = $cat_string . ',' . $nav ['name'] . '|' . $nav ['id'];
		}
		echo $cat_string;

	}
	function selectlist() {
		echo ("<select>");
		$cid = intval ( $this->uri->segment ( 3 ) ) ? $this->uri->segment ( 3 ) : 'all';
		$navlist = $this->category_model->list_by_grade (); //获取导航
		$cat_string = '';
		foreach ( $navlist as $nav ) {

			echo "<option value='" . $nav ['id'] . "'>" . $nav ['name'] . "</option>";
			if ($nav ['sublist'] != null) {
				foreach ( $nav ['sublist'] as $nav1 ) {
					echo "<option value='" . $nav1 ['id'] . "'>--" . $nav1 ['name'] . "</option>";
				}
			}

		}
		echo ("</select>");

	}

}

?>