<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

class Asynsendemail extends CI_Controller {

	var $whitelist;
	function __construct() {
		$this->whitelist = "msend";
		parent::__construct ( );

	}
/**

* 异步发送邮件

* @date: 2018年7月6日 上午9:29:58

* @author: 61703

* @param: 空

* @return:

*/
	function msend() {

		$tousername = $this->input->post ('tousername',FALSE);
		$mailtitle =  $this->input->post ('mailtitle');
		$mailcontent = $this->input->post ('mailcontent');

		sendemailtouser ( $tousername, $mailtitle, $mailcontent );
			exit("ok");
	}

}