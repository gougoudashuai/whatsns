<!--{template header}-->
  <!--{eval $topiclist=$this->fromcache('hottopiclist');}-->
<div class="m_news">
<div class="weui-flex m_news_title">
    <div class="weui-flex__item">推荐热点</div>
    <div class=""><a href="{url topic/hotlist}"><i class="icon_more"></i></a></div>
</div>
    <ul class="items">
    <!--{loop $topiclist $index $topic}-->
    {if $index==0}
        <li class="item">
          <div class="weui-flex">
              <div class="imgthumb">
               <a href="{url topic/getone/$topic['id']}">  <img src="{$topic['image']}"></a> 
                 
              </div>
              <div class="weui-flex__item item_right">
                <p class="text">
                     <a href="{url topic/getone/$topic['id']}">{$topic['title']}</a> 
                </p>
                  <p class="meta">
                      <span>评论</span><span>{$topic['articles']}个</span>
                  </p>
              </div>
          </div>
        </li>
        {else}
            {if $index<=4}
        <li class="item">
          <i>.</i>          <a href="{url topic/getone/$topic['id']}"> {eval echo clearhtml($topic['title'],18);}</a> 
        </li>
          {/if}
        {/if}
     
        <!--{/loop}-->
    </ul>
</div>
<div class="qlists">
 <!--{eval $vnosolvelist=$this->fromcache('nosolvelist');}--> 
 <!--{loop $vnosolvelist $index $question}-->
    <!--多图文-->
<div class="qlist">
<div class="title weui-flex">
    <div>
    {if $question['hidden']==1}
        <img src="{SITE_URL}static/css/default/avatar.gif">
        {else}
        <img src="{$question['avatar']}">
        {/if}
    </div>
    <div class="weui-flex__item">
       {if $question['hidden']==1}
         <span class="author">匿名用户 </span> 
       
          {else}
          <span class="author">{$question['author']}   {if $question['author_has_vertify']!=false}
        <i class="fa fa-vimeo {if $question['author_has_vertify'][0]=='0'}v_person {else}v_company {/if}  " data-toggle="tooltip" data-placement="right" title="" {if $question['author_has_vertify'][0]==0}data-original-title="个人认证" {else}data-original-title="企业认证" {/if} ></i>{/if}
         </span>
       
            {/if}
    </div>

</div>
    <p class="qtitle"><a href="{url question/view/$question['id']}">{$question['title']}</a></p>
   
   {if count($question['images'][1])>0}  
<div class="weui-flex">
 {if count($question['images'][1])>1}
 {loop  $question['images'][1] $index $img}
  {if  $index<=2}
    <div class="weui-flex__item"><div class="imgthumbsmall"> <a href="{url question/view/$question['id']}"><img class="lazy" src="{SITE_URL}static/images/lazy.jpg" data-original="{$img}"></a></div></div>
    {/if}
 {/loop}
 {else}
  {if $question['image']!=null&&count($question['images'][1])<=1}
  {loop  $question['images'][1] $index $img}
   <div class="weui-flex__item"><div class="imgthumbbig"><a href="{url question/view/$question['id']}"><img class="lazy" src="{SITE_URL}static/images/lazy.jpg" data-original="{$img}"></a></div></div>
   {/loop}
  {/if}
 {/if}
</div>
 {/if}
<p class="description">

 <a href="{url question/view/$question['id']}">{eval echo clearhtml($question['description'],50);} </a>
</p>
    <p class="meta">
       <span>
          <i class="icon_huida"></i>{$question['answers']}
       </span>
        <span>
           <i class="icon_liulan"></i> {$question['views']}
       </span>
     
          <span class="fr_gengduo" onclick="shownoview(this)">
           <i class="icon_more"></i> 
       </span>
       <div class="noview hideel" dataid="{$question['id']}" onclick="hidecontent(this)">
       不感兴趣
       </div>
    </p>
</div>
   
   
  <!--{/loop}-->
</div>
<div class="loadtext"></div>
<script type="text/javascript">
var noviewarr = [];
if(localStorage.getItem('noview')){
	noviewarr = JSON.parse(localStorage.getItem('noview'));
}


function shownoview(target){
	$(".hideel").hide();
	$(target).parent().parent().find(".hideel").toggle();
}
function hidecontent(target){
	
	var _id=$(target).attr("dataid");
	noviewarr.push(_id);
	localStorage.setItem('noview',JSON.stringify(noviewarr));
	$(target).parent().remove();
}

$(".noview").each(function(){
	
	var _id=$(this).attr("dataid");
	for(var i=0;i<noviewarr.length;i++){
        if(noviewarr[i]==_id){
        	$(this).parent().remove();
            }
	}
});
var page=2;
var end=false;
$(function(){
    var totalHeight = 0; //定义一个总高度变量
    function loadmore(){ //loa动态加载数据
        totalHeight =  parseFloat( $(window).height() ) +  parseFloat( $(window).scrollTop() ); //浏览器的高度加上滚动条的高度
        
        if ( $(document).height() <= totalHeight) { //当文档的高度小于或者等于总的高度时，开始动态加载数据
            console.log("加载中..");
            if(end==true){
                return false;
            }
            $.ajax({
    	        //提交数据的类型 POST GET
    	        type:"POST",
    	        //提交的网址
    	        url:"{url Interfacequestion/getnewquestion}",
    	        //提交的数据
    	        data:{datapage:page},
    	        //返回数据的格式
    	        datatype: "text",//"xml", "html", "script", "json", "jsonp", "text".
    	      //在请求之前调用的函数
    	        beforeSend:function(){
    	            $(".loadtext").html("加载中....").show();
    	        },
    	        //成功返回之后调用的函数
    	        success:function(response){

    	        	  if(response=='0'){
    	        		  end=true;
    	                  $(".loadtext").html("我也是有底限的!").show();
    	                  return false;
    	          	  }else{
    	          		   $(".qlists").append(response);
    	          		 $(".loadtext").html("").hide();
    	          
    	          	  }
    	          	
    	          	
    	          	   page++;
    	        }   ,

    	        //调用执行后调用的函数
    	        complete: function(XMLHttpRequest, textStatus){
    	        	
    	        },

    	        //调用出错执行的函数
    	        error: function(){
    	        	 $(".loadtext").html("").hide();
    	            //请求出错处理
    	        }
    	    });
    	    
        
        }
    }
    $(window).scroll(function(){
    	 loadmore();
    	$(".hideel").hide();
 
    })
   
})
</script>


<!--{template footer}-->
