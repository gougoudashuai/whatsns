<!--{template meta}-->
<style>
.newattention .current:after {
    content: "";
    position: absolute;
    bottom: 0px;
    width: .8rem;
    left: .52rem;
    border-bottom: solid 1px #ff973c;
}
</style>
 <div class="m_search userspaceheader">

    <div class="weui-flex newattention">
      
        <div class="weui-flex__item current"> <span class="ws_h_title"><a href="{url new/default}">问答</a></span></div>

         <div class="weui-flex__item "> <span class="ws_h_title"><a href="{url topic/default}">文章</a></span></div>
    </div>
</div>


<div class="qlists">
 <!--{loop $questionlist $index $question}-->
    <!--多图文-->
<div class="qlist">
<div class="title weui-flex">
    <div>
    {if $question['hidden']==1}
        <img src="{SITE_URL}static/css/default/avatar.gif">
        {else}
        <img src="{$question['avatar']}">
        {/if}
    </div>
    <div class="weui-flex__item">
       <span class="fr_gengduo" onclick="shownoview(this)">
           <i class="icon_more"></i> 
            <div class="noview hideel" dataid="{$question['id']}" onclick="hidecontent(this)">
       不感兴趣
       </div>
       </span>
      
       {if $question['hidden']==1}
         <span class="author">匿名用户 </span> 
       
          {else}
          <span class="author">{$question['author']}   {if $question['author_has_vertify']!=false}
        <i class="fa fa-vimeo {if $question['author_has_vertify'][0]=='0'}v_person {else}v_company {/if}  " data-toggle="tooltip" data-placement="right" title="" {if $question['author_has_vertify'][0]==0}data-original-title="个人认证" {else}data-original-title="企业认证" {/if} ></i>{/if}
         </span>
       
            {/if}
   
    {if $question['askuid']>0}  <span class="span_yaoqing"><i class="icon_yqhd"></i></span>  {/if}
    
  {if $question['hasvoice']!=0}<span	class=""><i class="icon_voice"></i></span> {/if}
    </div>

</div>
    <p class="qtitle"><a href="{url question/view/$question['id']}">{$question['title']}</a></p>
   
   {if count($question['images'][1])>0}  
<div class="weui-flex">
 {if count($question['images'][1])>1}
 {loop  $question['images'][1] $index $img}
  {if  $index<=2}
    <div class="weui-flex__item"><div class="imgthumbsmall"> <a href="{url question/view/$question['id']}"><img src="{$img}"></a></div></div>
    {/if}
 {/loop}
 {else}
  {if $question['image']!=null&&count($question['images'][1])<=1}
  {loop  $question['images'][1] $index $img}
   <div class="weui-flex__item"><div class="imgthumbbig"><a href="{url question/view/$question['id']}"><img src="{$img}"></a></div></div>
   {/loop}
  {/if}
 {/if}
</div>
 {/if}
<p class="description">

 <a href="{url question/view/$question['id']}">{eval echo clearhtml($question['description'],50);} </a>
</p>
    <p class="meta">
       <span>
          <i class="icon_huida"></i>{$question['answers']}
       </span>
        <span>
           <i class="icon_liulan"></i> {$question['views']}
       </span>
 
         {if $question['shangjin']!=0}  
         
         
           <span class="shangval "><i class="icon_shang"></i><span class="val">$question['shangjin']元</span></span>
           {/if}
             {if $question['price']!=0} 
           <span class="shangval jifenzhi"><i class="icon_jifen"></i><span class="val">$question['price']积分</span></span>
            {/if}
                   
       
    </p>
</div>
   
   
  <!--{/loop}-->
</div>
   <div class="pages">
                           {$departstr}
                        </div>
<div class="bottomspan">

</div>
<script type="text/javascript">
var noviewarr = [];
if(localStorage.getItem('noview')){
	noviewarr = JSON.parse(localStorage.getItem('noview'));
}


function shownoview(target){
	$(".hideel").hide();
	$(target).find(".hideel").toggle();
}
function hidecontent(target){
	
	var _id=$(target).attr("dataid");
	noviewarr.push(_id);
	localStorage.setItem('noview',JSON.stringify(noviewarr));
	$(target).parent().parent().parent().parent().remove();
}

$(".noview").each(function(){
	
	var _id=$(this).attr("dataid");

	for(var i=0;i<noviewarr.length;i++){
	
        if(noviewarr[i]==_id){
        	$(this).parent().parent().parent().parent().remove();
            }
	}
});
</script>
<!--{template footer}-->