<!--{template meta}-->
    <style>
        body{
            background: #f1f5f8;
        }
             .au_login_panelform{
        margin-top:.53rem
        }
    </style>


 <div class="m_search">

    <div class="weui-flex">
        <div><i onclick="window.history.go(-1)" class="fa fa-angle-left"></i></div>
        <div class="weui-flex__item"> <span class="ws_h_title">用户登录</span></div>
        <div><span class="ws_ab_reg" onclick="window.location.href='{url user/register}'"><i class="fa fa-user-o"></i>注册</span></div>
    </div>
</div>
  <div class="au_login_panelform sign">
        <form id="new_session" accept-charset="UTF-8" method="post">
         <input type="hidden"  id="forward" name="return_url" value="{$forward}">
          <input type="hidden" id="tokenkey" name="tokenkey" value='{$_SESSION["logintokenid"]}'/>
            <!-- 正常登录登录名输入框 -->
            <div class="input-prepend restyle js-normal">
                <input placeholder="手机号或邮箱/用户名" type="text" id="xm-login-user-name">
                <i class="fa fa-user"></i>
            </div>

            <!-- 海外登录登录名输入框 -->

            <div class="input-prepend">
                <input placeholder="密码" type="password" id="xm-login-user-password">
                <i class="fa fa-lock"></i>
            </div>

            <div class="remember-btn">
                        
               
                   <input type="checkbox" disabled id="agreeitem" value="1" checked/>                <a id="showadreeitem" style="color: #333;font-size:14px;">同意隐私条款[查看]</a>
                
              
           
            </div>
            <div class="forget-btn">
                <a class="" href="{url user/getpass}">登录遇到问题?</a>

            </div>
                 
            <input type="button" id="login_submit" value="登录" class="sign-in-button">
        </form>
         <!--{if $setting['sinalogin_open']||$wxbrower||$setting['qqlogin_open']}-->
        <!-- 更多登录方式 -->
        <div class="more-sign hideapp">

            <h6>第三方登录</h6>
            <ul>
                <!--{if $setting['sinalogin_open']}-->
                <li><a class="weibo" href="{SITE_URL}plugin/sinalogin/index.php"><i class="fa fa-weibo"></i></a></li>
                <!--{/if}-->
               <!--{if $wxbrower}-->
                <li><a class="weixin" href="{SITE_URL}{$setting['seo_prefix']}plugin_weixin/wxauth"><i class="fa fa-wechat"></i></a></li>
                <!--{/if}-->
                <!--{if $setting['qqlogin_open']}-->
                <li><a class="qq" href="{SITE_URL}plugin/qqlogin/index.php"><i class="fa fa-qq"></i></a></li>
                <!--{/if}-->



            </ul>

        </div>
         <!--{/if}-->
    </div>
        <div class="modal fade" id="agreemodel" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="padding-top: 50px;">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">网站登录协议说明</h4>
      </div>

        <div class="modal-body">
                    <div style="height: 450px;overflow:scroll;">
                        <p>&nbsp; &nbsp; &nbsp; &nbsp;当您登录时，表示您已经同意遵守本规章。&nbsp;</p><p>欢迎您加入本站点参加交流和讨论，本站点为公共论坛，为维护网上公共秩序和社会稳定，请您自觉遵守以下条款：&nbsp;</p><p><br></p><p>一、不得利用本站危害国家安全、泄露国家秘密，不得侵犯国家社会集体的和公民的合法权益，不得利用本站制作、复制和传播下列信息：</p><p>　 （一）煽动抗拒、破坏宪法和法律、行政法规实施的；</p><p>　（二）煽动颠覆国家政权，推翻社会主义制度的；</p><p>　（三）煽动分裂国家、破坏国家统一的；</p><p>　（四）煽动民族仇恨、民族歧视，破坏民族团结的；</p><p>　（五）捏造或者歪曲事实，散布谣言，扰乱社会秩序的；</p><p>　（六）宣扬封建迷信、淫秽、色情、赌博、暴力、凶杀、恐怖、教唆犯罪的；</p><p>　（七）公然侮辱他人或者捏造事实诽谤他人的，或者进行其他恶意攻击的；</p><p>　（八）损害国家机关信誉的；</p><p>　（九）其他违反宪法和法律行政法规的；</p><p>　（十）进行商业广告行为的。</p><p><br></p><p>二、互相尊重，对自己的言论和行为负责。</p><p>三、禁止在申请用户时使用相关本站的词汇，或是带有侮辱、毁谤、造谣类的或是有其含义的各种语言进行注册用户，否则我们会将其删除。</p><p>四、禁止以任何方式对本站进行各种破坏行为。</p><p>五、如果您有违反国家相关法律法规的行为，本站概不负责，您的登录论坛信息均被记录无疑，必要时，我们会向相关的国家管理部门提供此类信息。</p>
                    </div>
                </div>


    </div>
  </div>
</div>
<script type="text/javascript">
$(".close").click(function(){
	$('#agreemodel').hide();
});
$("#showadreeitem").click(function(){
	$('#agreemodel').show();
}
);
</script>
 <script>
 var uname_tmp=window.localStorage.getItem("username");
 var upwd_tmp=window.localStorage.getItem("userpwd");
 if(uname_tmp!=null){
	 $("#xm-login-user-name").val(uname_tmp);
	 $("#xm-login-user-password").val(upwd_tmp);
	 $("#keeppwd").attr("checked",'true');
 }
 function keepuserinfo(){
	 var _pwdkeep=$("#keeppwd").attr("checked");
	 var _uname=$("#xm-login-user-name").val();
	    var _upwd=$("#xm-login-user-password").val();
	if(_pwdkeep){
		window.localStorage.setItem("username",_uname);
		window.localStorage.setItem("userpwd",_upwd);
	}else{

		window.localStorage.removeItem("username");
		window.localStorage.removeItem("userpwd");
	}
 }
 $("#keeppwd").change(function(){
	 keepuserinfo();
 })
$("#login_submit").bind("click",function(event){
	 var _forward=$("#forward").val();
    var _uname=$("#xm-login-user-name").val();
    var _upwd=$("#xm-login-user-password").val();
    var _apikey=$("#tokenkey").val();
    var _code=$("#seccode_verify").val();
    var el='';
    {if $_SESSION ['authinfo']}

    var _auth=1;
    {else}
    var _auth=0;
    {/if}
    $.ajax({
        //提交数据的类型 POST GET
        type:"POST",
        //提交的网址
        url:"{url api_user/loginapi}",
        //提交的数据
        data:{uname:_uname,upwd:_upwd,apikey:_apikey,seccode_verify:_code},
        //返回数据的格式
        datatype: "text",//"xml", "html", "script", "json", "jsonp", "text".
      //在请求之前调用的函数
        beforeSend:function(){
     	    el=$.loading({
     	        content:'加载中...',
     	    })
        },
        //成功返回之后调用的函数
        success:function(data){

        	 el.loading("hide");
            if(data=='login_ok'){

            	keepuserinfo();




             	var _forward="{$forward}"; //user/logout
             	if(_auth==1||_forward.indexOf('user/logout')>=0||_forward.indexOf('user/checkemail')>=0){
             		window.location.href="{url index}";
             	}else{
             		window.location.href="{$forward}";
             	}






            }else{
            	  switch(data){
            	  case 'login_null':

            		  el2=$.tips({
            	            content:'用户名或者密码为空',
            	            stayTime:1000,
            	            type:"info"
            	        });
            		  break;
 case 'login_user_or_pwd_error':

	  el2=$.tips({
          content:'用户名或者密码错误',
          stayTime:1000,
          type:"info"
      });
            		  break;
default:

el2=$.tips({
    content:data,
    stayTime:1000,
    type:"info"
});
	break;
            	  }
            }
        }   ,

        //调用执行后调用的函数
        complete: function(XMLHttpRequest, textStatus){
     	    el.loading("hide");
        },

        //调用出错执行的函数
        error: function(){
            //请求出错处理
        }
    });
    event.stopPropagation();    //  阻止事件冒泡
    return false;
});


</script>
