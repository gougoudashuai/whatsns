<!--{template header}-->
<section class="ui-container">



                  <!--话题介绍-->
                    <div class="au_category_huati_info">
                           <div class="ui-row-flex">
                               <div class="ui-col">
                                   <div class="au_category_huati_img">
                                    <a href="{url topic/catlist$catmodel['id']}">
                                       <img src="$catmodel['bigimage']">
                                       </a>
                                   </div>
                               </div>
                               <div class="ui-col ui-col-3">
                                   <div class="au_category_huati_name"> <a  href="{url topic/catlist$catmodel['id']}"> {$catmodel['name']}</a></div>
                                   <div class="au_category_info_meta">
                                       <div class="au_category_info_meta_item"><i class="fa fa-question-circle hong"></i>{$catmodel['questions']}个问题</div>
                                       <div class="au_category_info_meta_item"><i class="fa fa-user lan"></i>{$catmodel['followers']}人关注</div>
                                       <div class="au_category_info_meta_item"><i class="fa fa-file-text-o ju "></i>{$trownum}篇文章</div>
                                   </div>
                               </div>
                         
                           </div>

                        <!--子话题-->
                        {if $catlist}
                        <div class="ui-row-flex">
                            <div class="i-col ui-col  au_category_info_childlist">

                                <div class="swiper-container" >
                                    <div class="swiper-wrapper">
                                        <!--{loop $catlist $index $cat}-->
                                        
                                        <div class="swiper-slide" data-swiper-autoplay="2000">
                                         
                                            
                                                    <div class="au_category_info_child">
                                                        <a href="{url topic/catlist/$cat['id']}">
                                                        <div class="au_category_info_child_img">
                                                            <img src="$cat['image']">
                                                        </div>
                                                        <p class="au_category_info_child_text">{$cat['name']}</p>
                                                        </a>
                                                    </div>
                                                

                                           
                                        </div>
                                  <!--{/loop}--> 
                                    </div>

                                </div>
                                <!-- 如果需要导航按钮 -->
                                <div class="swiper-button-prev"></div>
                                <div class="swiper-button-next"></div>



                            </div>

                        </div>
                       {/if}
                    </div>
                  
                     <!--导航提示-->
                    <div class="au_brif wzbrif">
                        <span class="au_bref_item current "> 
                           <a href="{url topic/catlist/$cid}">全部文章</a> </span>
                               {if $catmodel['isuseask']}
                         <span class="au_bref_item">
                         <a href="{url category/view/$catmodel['id']}">相关讨论</a>
  </span> {/if}
                    </div>
                       <!--列表部分-->
                  
              <div class="qlists">
      <!--{loop $topiclist $index $topic}-->   
    <!--多图文-->
<div class="qlist">
<div class="title weui-flex">
    <div>
   
        <img src="{$topic['avatar']}">
        
    </div>
    <div class="weui-flex__item">

          <span class="author">{$topic['author']}   {if $topic['author_has_vertify']!=false}
        <i class="fa fa-vimeo {if $topic['author_has_vertify'][0]=='0'}v_person {else}v_company {/if}  " data-toggle="tooltip" data-placement="right" title=""  ></i>{/if}
         </span>
       
          
    </div>

</div>
    <p class="qtitle"><a href="{url topic/getone/$topic['id']}">{$topic['title']}</a></p>
   
   {if count($topic['images'][1])>0}  
<div class="weui-flex">
 {if count($topic['images'][1])>1}
 {loop  $topic['images'][1] $index $img}
 {if  $index<=2}
    <div class="weui-flex__item"><div class="imgthumbsmall"> <a href="{url topic/getone/$topic['id']}"><img src="{$img}"></a></div></div>
    {/if}
 {/loop}
 {else}
  {if $topic['image']!=null&&count($topic['images'][1])<=1}
  {loop  $topic['images'][1] $index $img}
   <div class="weui-flex__item"><div class="imgthumbbig"><a href="{url topic/getone/$topic['id']}"><img src="{$img}"></a></div></div>
   {/loop}
  {/if}
 {/if}
</div>
 {/if}
<p class="description">
 <a href="{url topic/getone/$topic['id']}" style="color:#333"> 
      {if $topic['price']!=0}
                                              <div class="box_toukan ">
  {eval echo clearhtml($topic['freeconent'],50);}
  {if $topic['readmode']==2}
  
											<a  class="thiefbox font-12" ><i class="icon icon-lock font-12"></i> &nbsp;更多阅读需支付&nbsp;$topic['price']&nbsp;&nbsp;积分……</a>

   {/if}
        {if $topic['readmode']==3}		                      
											<a  class="thiefbox font-12" ><i class="icon icon-lock font-12"></i> &nbsp;更多阅读需支付&nbsp;$topic['price']&nbsp;&nbsp;元……</a>

		                    {/if}
										</div>
                   {else}
                   {eval echo clearhtml($topic['describtion'],50);}

                    {/if}
                    </a>
</p>
    <p class="meta">
       <span>
          <i class="icon_huida"></i>{$topic['articles']}
       </span>
        <span>
           <i class="icon_liulan"></i> {$topic['views']}
       </span>
    </p>
</div>
   
   
  <!--{/loop}-->
</div>
               <div class="pages">{$departstr}</div>
                    








<script>
    var swiper = new Swiper('.swiper-container', {
        loop:true,
        autoplay:2000,
        slidesPerView: 3,
        paginationClickable: true,
        spaceBetween: 10,
        // 如果需要前进后退按钮
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });
    var intswper=setInterval("swiper.slideNext()", 2000);
    $(".swiper-container").hover(function(){
        clearInterval(intswper);
    },function(){
        intswper=setInterval("swiper.slideNext()", 2000);
    })
</script>
</section>
<!--{template footer}-->