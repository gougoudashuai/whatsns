# whatsns内容付费seo优化带采集和熊掌号运营问答系统

## 项目介绍
whatsns问答系统是一款可以根据自身业务需求快速搭建垂直化领域的php开源问答系统。

- 内置强大的采集功能，
- 支持云存储，
- 图片水印设置，
- 全文检索，
- 站内行为监控，
- 短信注册和通知，
- 伪静态URL自定义，
- 熊掌号功能，
- 百度结构化地图（标签，问题，文章，分类，用户空间），
- PC和Wap模板分离，
- 内置多套pc和wap模板，站长自由切换，
- 同时后台支持模板管理在线编辑修改模板，
- 强大的防灌水拦截和过滤配置等上百项功能，
- 深入SEO优化，适合对SEO有需求的站长。


商业版还支持火车采集，高级微信公众号接口功能，支持支付宝支付、微信扫码支付、微信JSSDK支付、微信H5支付、小程序支付、适合不同场景支付业务需求，如充值，打赏，回答偷看，付费专家咨询。 

## 软件架构

基于php的CodeIgniter3.1.6开发
优雅的CI框架是国内php开发者最喜欢的MVC框架，上手快，轻量级，可以在虚拟主机单核cpu1g内存1m带宽下完美流畅运行，可以参考CI官网了解更多框架信息http://codeigniter.org.cn/


## 安装教程


- 问答系统V3.8用户可以直接上传文件夹到问答根目录安装，域名访问即可，国内主机需备案后访问安装，
- IIS用户安装后如出现问答网页地址变成IP情况参考V3.7版本application/config/config.php配置文件里绑定域名。
- 问答系统3.7用户请先修改application/config/config.php配置文件

    

下一步直接域名访问安装即可，如果先安装后修改配置域名的用户会导致模板缓存文件中url地址错误，可以删除根目录下data/view/*.php和data/cache/*.php全部php缓存文件，记住只能删除php文件夹，这些都是缓存文件。

##  二级目录安装说明

二级目录安装请打开application/config/config.php。

```
$config['base_url'] ='http://localhost/';

$config['base_url'] ="http://你的域名/二级目录/";

```

打开 system\core\CodeIgniter.php

![输入图片说明](https://images.gitee.com/uploads/images/2018/1212/102758_7dce51a2_482269.png "屏幕截图.png")

取消上面的注释 删掉 //

```
     if($class=="二级目录名称--首字母大写，一定要大写首字母"){
   	$class="Index";
    }
```
如图：

![输入图片说明](https://images.gitee.com/uploads/images/2018/1212/102921_78ea8489_482269.png "屏幕截图.png")

## 使用说明

[数据表字典下载](https://pan.baidu.com/s/1mltRTGcWtj5IdfHTrOEpZw)
[开发文档](https://pan.baidu.com/s/1-o-SqwlHdlKo-QDMe0wEMA)
[邮箱配置教程](https://www.ask2.cn/article-14579.html)
[水印设置教程](https://www.ask2.cn/article-14580.html)
[伪静态教程](https://www.ask2.cn/article-14574.html)

#### 界面截图
![whatsns问答系统PC首页](https://images.gitee.com/uploads/images/2018/1207/101122_c53f2573_482269.png "屏幕截图.png")
![whatsns问答系统wap站首页](https://images.gitee.com/uploads/images/2018/1207/101151_fdcf7834_482269.png "屏幕截图.png")

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)